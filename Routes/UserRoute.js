const express = require('express')
const { getUser, getOne, updateUser, deleteUser } = require('../Controller/UserController.js')
const authenticateUser = require('../Middleware/authMiddleware.js')

const router = express.Router()

/**
 * @api {get} /user Récupère tous les utilisateurs
 * @apiName getUser
 * @apiGroup Users
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {Object[]} users Liste tous les utilisateur
 * @apiSuccess {String} name le nom de l'utilisateur
 * @apiSuccess {String} email l'eamil de l'utilisateur
 */
router.get('/user', getUser)

/**
 * @api {get} /user/:id récupère un utilisateur
 * @apiName getOne
 * @apiGroup Users
 * @apiVersion 1.0.0
 * 
 * @apiParam {String} id User's ID
 * 
 * @apiSuccess {String} name Nom de l'utilisateur
 * @apiSuccess {String} email L'email de l'utilisateur
 */
router.get('/user/:id', getOne)

/**
 * @api {put} /user/:id mettre à jour l'utilisateur
 * @apiName updateUser
 * @apiGroup Users
 * @apiVersion 1.0.0
 * 
 * @apiParam {String} id User's ID
 * 
 * @apiSuccess {String} name Le nom de l'utilisateur
 * @apiSuccess {String} email L'email de l'utilisateur
 * @apiSuccess {String} password Le mot de passe de l'utilisateur
 */
router.put('/user/:id', authenticateUser, updateUser)

/**
 * @api {delete} /user/:id supprimer un utilisateur
 * @apiName deleteUser
 * @apiGroup Users
 * @apiVersion 1.0.0
 * 
 * @apiParam {String} id User's ID
 * 
 * @apiSuccess {String} Message de confirmation de suppression
 */
router.delete('/user/:id', authenticateUser, deleteUser)

module.exports = router;