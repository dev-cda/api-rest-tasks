const User = require('../Model/User.js')
const bcrypt = require('bcrypt')


/**
 * Get all users
 * 
 * @route GET /user
 * @returns {Object[]} 200 - Array of users
 * @returns {String} 500 - Error message
 */
exports.getUser = async (req, res) => {

    try {
        
        let users = await User.find();

        users.map((user) => {
            const {password,...otherDetails} = user._doc

            return otherDetails
        })

        res.status(200).json(users)

    } catch (error) {

        res.status(500).json(error)
        
    }
}


/**
 * Get user by Id
 * 
 * @route GET /user/{id}
 * @param {String} id.path.required - User ID
 * @returns {Object} 200 - User Object
 * @returns {String} 404 - No such user exists error message
 * @returns {String} 500 - Error message
 */
exports.getOne = async (req, res) => {

    const id = req.params.id

    try {
        
        const user = await User.findById(id)

        if (user)
        {
            const { password, ...otherDetails} =user._doc

            res.status(200).json(otherDetails)
        }
        else
        {
            res.status(404).json('No such user exists')
        }

    } catch (error) {
        
        res.status(500).json(error)
    }
}


/**
 * Update a user by Id
 * 
 * @route PUT /user/{id}
 * @param {String} id.path.required - User ID
 * @param {String} name.body - Updated user name
 * @param {String} email.body - Updated user email
 * @param {String} password.body - Updated user password
 * @returns {Object} 200 - Upadted user object
 * @returns {String} 500 - Server error message
 */
exports.updateUser = async (req, res) => {

    const { id } = req.params
    const { name, email, password } = req.body

    try {
        
        let user = await User.findById(id)

        if (!user)
        {
            return res.status(404).json({ message: 'User not found'})
        }

        user.name = name
        user.email = email
        user.password = await bcrypt.hash(password, 10)

        await user.save()

        res.status(200).json({ message: 'User updated successfully'})

    } catch (error) {

        console.error(error);
        res.status(500).json({ message: 'Server error' })
        
    }
}


/**
 * Delete a user by ID
 * 
 * @route DELETE /user/{id}
 * @param {String} id.path.required - User ID
 * @returns {String} 404 - User not found error message
 * @returns {String} 200 - User deleted successfully message
 * @returns {String} 500 - Server error message
 */
exports.deleteUser = async (req, res) => {

    const { id } = req.params

    try {
        
        const deleteUser = await User.findOneAndDelete({ _id: id})

        if (!deleteUser)
        {
            return res.status(404).json({ message: 'User not found'})
        }

        res.status(200).json({ message: 'User deleted successfully'})

    } catch (error) {

        console.error(error);
        res.status(500).json({ message: 'Server error'})
        
    }
}