const jwt = require('jsonwebtoken')
require('dotenv').config();

const authenticateUser = (req, res, next) => {

    const token = req.headers.authorization

    if (!token)
    {
        return res.status(401).json({ error: 'Missing token' })
    }

    jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {

        if (err)
        {
            return res.status(401).json({ error: 'Invalid token' })
        }

        req.userId = decodedToken.userId
        next()
    })
}

module.exports = authenticateUser;