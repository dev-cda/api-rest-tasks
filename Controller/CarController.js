const mongoose = require('mongoose');
const Car = require('../Model/Car.js');



/**
 * Create new Product
 * 
 * @route POST /car
 * @param {String} req.body.required - marque, modele, carrosserie, couleur, carburant, moteur
 * @param {Number} req.body.required - puissance, vitesse
 * @param {Float} req.body.required - acceleration
 * @returns {Object} 200 - Created Car Object
 * @returns {String} 400 - error message
 */
exports.createProduct = async (req, res) => {
    const newCar = new Car({
        marque: req.body.marque,
        modele: req.body.modele,
        carrosserie: req.body.carrosserie,
        couleur: req.body.couleur,
        carburant: req.body.carburant,
        moteur: req.body.moteur,
        puissance: req.body.puissance,
        acceleration: req.body.acceleration,
        vitesse: req.body.vitesse
    })

    try {
        const dataToSave = await newCar.save();
        res.status(200).json(dataToSave);

    } catch (error) {
        res.status(400).json({ message: error.message })
    }
};


/**
 * Get all Cars
 * 
 * @route GET /car
 * @returns {Object[]} 200 - Array of cars
 * @returns {String} 500 - error message
 */
exports.getProduct = async (req, res) => {
    try {
        const data = await Car.find();
        res.json(data)

    } catch (error) {
        res.status(500).json({ message: error.message })
    }
};


/**
 * Get a car by ID
 * 
 * @route GET /car/{id}
 * @param {String} id.path.required - Car ID
 * @returns {Object} - Car object
 * @returns {String} 500 - error message
 */
exports.getOne = async (req, res) => {
    try {
        const data = await Car.findById(req.params.id);
        res.json(data)

    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}


/**
 * Update by ID
 * 
 * @route PUT /car/{id}
 * @param {String} id.path.required - Car ID
 * @param {String Number} req.body - Update Car Data
 * @returns {Object} result - Updated Car object
 * @returns {String} 400 - Error message
 */
exports.updateProduct = async (req, res) => {
    try {
        const id = req.params.id;
        const updateData = req.body;
        const options = { new: true }

        const result = await Car.findByIdAndUpdate(
            id, updateData, options
        )

        res.send(result)

    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}


/**
 * Delete by ID
 * 
 * @route DELETE /car/{id}
 * @param {string} id.path.required - Car ID
 * @returns {String} - message has been deleted
 * @returns {String} 400 - Error message
 */
exports.deleteProduct = async (req, res) => {
    try {
        const id = req.params.id;
        const data = await Car.findByIdAndDelete(id)

        res.send(`Product with ${data.name} has been deleted...`)

    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}