
const User = require('../Model/User.js')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')


/**
 * Register a user
 * 
 * @route POST /register
 * @param {String} name.body.required - User name
 * @param {String} email.body.required - User email
 * @param {String} password.body.required - User password
 * @returns {Object} 201 - Created user object
 * @returns 400 - Error message
 */
exports.signup = async (req, res) => {

    try {

        const { name, email, password } = req.body
        const hashedPassword = await bcrypt.hash(password, 10)

        const user = new User({ name, email, password: hashedPassword })

        await user.validate()
        await user.save()

        res.status(201).json({ message: 'User registered successfully '})
        
    } catch (error) {

        if (error.errors && error.errors.name) 
        {
            return res.status(400).json({ error: 'Name validation failed' });
        }
        
        if (error.code === 11000)
        {
            return res.status(400).json({ error: 'Email address already exists'})
        }

        if (error.errors && error.errors.password) 
        {
            return res.status(400).json({ error: 'Password validation failed' });
        }

        res.status(400).json({ error: error.message })
    }

}


/**
 * Authenticate and login user
 * 
 * @route POST /login
 * @param {String} email.body.required - User email
 * @param {String} password.body.required - User password
 * @returns {String} 400 - User not found error message
 * @returns {String} 401 - Invalid password error message
 * @returns {String} RES - JSON(Token)
 * @returns {String} 500 - Server error message
 */
exports.siging = async (req, res) => {

    try {

        const { email, password } = req.body

        const user = await User.findOne({ email });

        if (!user)
        {
            return res.status(400).json({ error: 'User not found'})
        }

        const isPasswordValid = await bcrypt.compare(password, user.password)

        if (!isPasswordValid)
        {
            return res.status(401).json({ error: 'Invalid password'})
        }

        const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET)

        res.json({ token })
        
    } catch (error) {

        res.status(500).json({ error: 'Server error' })
        
    }

}