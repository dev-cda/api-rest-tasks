const express = require('express')
const router = express.Router();

const authenticateUser = require('../Middleware/authMiddleware.js');
const { createProduct, getProduct, getOne, updateProduct, deleteProduct } = require('../Controller/CarController.js')


/**
 * @api {post} /api/car créer une nouvelle voiture
 * @apiName createCar
 * @apiGroup Cars
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {String} marque la marque de la voiture
 * @apiSuccess {String} modele le modele de la voiture
 * @apiSuccess {String} carrosserie la carrosserie de la voiture
 * @apiSuccess {String} couleur la couleur de la voiture
 * @apiSuccess {String} carburant carburant de la voiture
 * @apiSuccess {String} moteur le moteur de la voiture
 * @apiSuccess {Number} puissance la puissance de la voiture
 * @apiSuccess {Float} acceleration accelartion du 0 à 100 de la voiture
 * @apiSuccess {Number} vitesse la vitesse de la voiture
 */
router.post('/car', authenticateUser, createProduct);

/**
 * @api {get} /api/car liste tout les modele de voiture
 * @apiName getCar
 * @apiGroup Cars
 * @apiVersion 1.0.0
 */
router.get('/car', getProduct);

/**
 * @api {get} /car/:id récupère une seule voiture par l'ID
 * @apiName getOne
 * @apiGroup Cars
 * @apiVersion 1.0.0
 * 
 * @apiParam {String} id Cars ID
 * 
 */
router.get('/car/:id', authenticateUser, getOne);

/**
 * @api {put} /car/:id Mettre à jour une voiture
 * @apiName updateProduct
 * @apiGroup Cars
 * @apiVersion 1.0.0
 * 
 * @apiParam {String} id Cars ID
 * @apiParam {String} marque
 * @apiParam {String} modele
 * @apiParam {String} carrosserie
 * @apiParam {String} couleur
 * @apiParam {String} carburant
 * @apiParam {String} moteur
 * @apiParam {Number} puissance
 * @apiParam {Float} acceleration
 * @apiParam {Number} vitesse
 */
router.put('/car/:id', authenticateUser, updateProduct);

/**
 * @api {delete} /car/:id supprimer une voiture
 * @apiName deleteProduct
 * @apiGroup Cars
 * @apiVersion 1.0.0
 * 
 * @apiParam {String} id Cars ID
 * 
 * @apiSuccess {String} message de confirmation de suppression
 */
router.delete('/car/:id', authenticateUser, deleteProduct);


module.exports = router;