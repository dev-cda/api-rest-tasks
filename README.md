# DOCUMENTATION

## INTRODUCTION

L'API permet de lister les différentes modèle de voiture, l'utilisateur pourra s'enregistre et se connecter, 
l'API contient un middleware pour les actions POST UPDATE & DELETE pour les voitures et les utilisateurs (sachant que pour créer un utilisateur l'utilisateur doit s'enregistrez)

## ENDPOINTS

### VOITURES

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| GET | /api/car | Permet de récupérer tous les voitures |
| GET | /api/car/:id | Permet de récupérer une voitures par son ID |
| POST | /api/car | Permet de créer une nouvelle voiture |
| PUT | /api/car/:id | Permet de mettre à jour une voiture |
| DELETE | /api/car/:id | Permet de supprimer une voiture |


### UTILISATEURS

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| GET | /api/user | Permet de récupérer tous les utilisateurs |
| GET | /api/user/:id | Permet de récupérer un utilisateur par son ID |
| PUT | /api/user/:id | Permet de mettre à jour un utilisateur |
| DELETE | /api/user/:id | Permet de supprimer un utilisateur |

## AUTHENTIFICATION

L'utilisateur pourra s'enregistrer (créer un profile) en renseignant son nom, email et mot de passe (les mots de passe enregistrer seront hasher),
par la suite l'utilisateur pourra s'authentifier avec son email et son mot de passe, une fois authentifier un jeton d'accès token lui sera générer.

|  HTTP| ROUTE | DESCRIPTION |
|--|--|--|
| POST | /api/register | Permet de s'enregistrer |
| POST | /api/login | Permet de se connecter et renvoi un jeton d'accès token |


## TEST UNITAIRES

Commande pour exécuter les test unitaires de l'API

```
npm test
```

## GENERATION D'APIDOC

Command pour générer la documentation de l'API avec apidoc

```
npm run apidoc
```

## INSTALLATION

1. Clonez le référentiel

```
git clone https://gitlab.com/dev-cda/api-rest-tasks.git
```

2. Accédez au répertoire du projet

```
cd votre-projet
```

3. Installez les dépendances requises

```
npm install
```

4. Configurez les variables d'environnement nécessaires, telles que les informations de base de données, clé d'API

5. Démarrez le serveur de développement

```
npm start
```

Assurez-vous d'avoir Node.js et npm installés sur votre machine.




