
const express = require('express');
const cors = require('cors')
const mongoose = require('mongoose');

const app = express();


const CarRoute = require('./Routes/CarRoute.js')
const AuthRoute = require('./Routes/AuthRoute.js')
const UserRoute = require('./Routes/UserRoute.js')

require('dotenv').config();


const PORT = process.env.PORT || 3000;
const mongoString = process.env.DATABASE_URL;


// DATABASE CONNECTION
mongoose.connect(mongoString);
const database = mongoose.connection

database.on('error', (error) => {
	console.log(error);
});

database.once('connected', () => {
	console.log('Database connected');
});

app.use(cors());

app.use(express.json());


// Routes
app.use('/api', CarRoute);
app.use('/api', AuthRoute);
app.use('/api', UserRoute);


// Port Listennig
const server = app.listen(PORT, () => {
	console.log(`Server running on PORT ${PORT}`);
});

module.exports = server
