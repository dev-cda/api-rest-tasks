
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')


chai.use(chaiHttp)
const expect = chai.expect

describe('UserController', () => {
    describe('GET /api/user', () => {
        it('should return all users', function (done) {
            this.timeout(5000)
            chai.request(server)
                .get('/api/user')
                .end((err, res) => {
                    expect(res).to.have.status(200)
                    expect(res.body).to.be.an('array')
                    done()
                })
        }).timeout(5000)
    })

    describe('GET /api/user/:id', () => {
        it('should return a specific user', function (done) {
            const userId = '647a00e43c56f60f81e13d5e'

            this.timeout(5000)
            chai.request(server)
                .get(`/api/user/${userId}`)
                .end((err, res) => {
                    expect(res).to.have.status(200)
                    expect(res.body).to.be.an('object')
                    done()
                })
        }).timeout(5000)
    })
})