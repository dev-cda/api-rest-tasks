const express = require('express')
const { signup, siging } = require('../Controller/AuthController.js')

const router = express.Router()

/**
 * @api {post} /register Créer un nouveau utilisateur
 * @apiName signup
 * @apiGroup Users
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {String} name Le nom de l'utilisateur
 * @apiSuccess {String} email L'email de l'utilisateur
 * @apiSuccess {String} password Le mot de passe de l'utilisateur
 */
router.post('/register', signup)

/**
 * @api {post} /login Connexion d'un utilisateur
 * @apiName siging
 * @apiGroup Authentification
 * @apiVersion 1.0.0
 * 
 * @apiSuccess {String} email Adresse email de l'utilisateur
 * @apiSuccess {String} password Mot de passe de l'utilisateur
 */
router.post('/login', siging)


module.exports = router;