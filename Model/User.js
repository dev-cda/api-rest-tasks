
const mongoose = require('mongoose')
const validator = require('validator');


const userSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
        validate: {
            validator: (value) => {
              return validator.isLength(value, { min: 2, max: 50 });
            },
            message: 'Name must be between 2 and 50 characters long.'
          }
    },
    email: {
        type: String,
        unique: true,
        required: true,
        validate: {
            validator: (value) => {
                return validator.isEmail(value)
            },

            message: 'Invalid email address.'
        }
    },
    password: {
        type: String,
        required: true,
        validate: {
            validator: (value) => {
                return validator.isLength(value, { min: 6 });
              },
              message: 'Password must be at least 6 characters long.'
        }
    }

})



module.exports = mongoose.model('User', userSchema)