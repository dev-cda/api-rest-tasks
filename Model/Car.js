const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    marque: {
        required: true,
        type: String
    },
    modele: {
        required: true,
        type: String
    },
    carrosserie: {
        required: true,
        type: String
    },
    couleur: {
        type: String,
        required: true
    },
    carburant: {
        type: String
    },
    moteur: {
        required: true,
        type: String
    },
    puissance: {
        required: true,
        type: Number,
        default: 0
    },
    acceleration: {
        required: true,
        type: mongoose.Types.Decimal128
    },
    vitesse: {
        required: true,
        type: Number,
        default: 0
    }
},
{
    timestamps: true
});

module.exports = mongoose.model('Cars', productSchema)